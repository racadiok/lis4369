> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### Assignment 5 Requirements:

1. Use Assignment 5 screenshots and references above for the following requirements:
2. Backward-engineer the **lis4369_p2_requirements.txt** file.
3. Be sure to include **at least two plots** in your **README.md file**.
4. Be sure to test your program using *RStudio*.


#### README.md file should include the following items:

   * **Assignment requirements, as per A6**.
   * Screenshots of output from code below.

#### Assignment Screenshots:

Screenshot of Rstudio lis4369_a6.R running:

![image 1](images/1.png)

Screenshot of qplot:

![image 2](images/2.png)

Screenshot of gplot:

![image 3](images/3.png)