> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### Assignment 1 Requirements:

*Four Parts:*

   1. Distributed Version Control with Git and Bitbucket
   2. Development Installations
   3. Questions
   4. Bitbucket repo links:
      a) this assignment and
      b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

   * Screenshot of a1_tip_calculator application running
   * git commands w/short descripts

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

   1. git init - creates new Git repository
   2. git status - displays the state of the working directory and staging area
   3. git add - adds file contents to the index
   4. git commit - used to save your changes to the local repository
   5. git push - used to upload local repositorycontent to a remote repository
   6. git pull - fetch from and integrate with another repository or local branch
   7. git stash - stash changes in a dirty working directory

#### Assignment Screenshots:

Screenshot of a1_tip_calculator application running (IDLE):

![image 1](images/TipCalcIDLE.png)

Screenshot of a1_tip_calculator application running (Visual Studio Code):

![image 2](images/TipCalcVS.png)