> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### Project 1 Requirements:

1. Requirements:
    a. Code and run demo.py
    b. Then, use it to backward-engineer the screenshots below it.
2. Be sure to test your program using both IDLE and Visual Studio Code.


#### README.md file should include the following items:

   * Assignment requirements, as per P1.
   * Screenshots as per examples below.

#### Assignment Screenshots:

Screenshots of demo.py application running:

![image 1](images/p1.png)

![image 2](images/2p1.png)

![image 3](images/3p1.png)

Graph:

![image 4](images/graphp1.png)