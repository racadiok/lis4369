> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### Assignment 3 Requirements:

 1. Calculate home interior paint cost (w/o primer).
 2. Must use float data types.
 3. Must use SQFT_PER_GALLON constant (350).
 4. Must use iteration structure (aka "loop").
 5. Format, right-align numbers, and round to two decimal places.
 6. Create at least three functions that are called by the program:
    a. main(): calls at least two other functions.
    b. get_requirements(): displays the program requirements.
    c. calculate_payroll(): calculates interior home painting.


#### README.md file should include the following items:

   * Assignment requirements, as per A3.
   * Screenshots as per examples below.

#### Assignment Screenshots:

Screenshot of a3_painting_estimator application running:

![image 1](images/a3_painting_estimator.png)

Second screenshot of a3_painting_estimator application running:

![image 2](images/a3_painting_estimator2.png)