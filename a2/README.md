> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### Assignment 2 Requirements:

 1. Must use float data type for user input.
 2. Overtime rate: 1.5 times hourly rate (hours over 40).
 3. Holiday rate: 2.0 times hourly rate (all holiday hours).
 4. Must format currency with dollar sign, and round to two decimal places.
 5. Create at least three functions that are called by the program:
    a. main(): calls at least two other functions.
    b. get_requirements(): displays the program requirements.
    c. calculate_payroll(): calculates an individual one-week paycheck.


#### README.md file should include the following items:

   * Assignment requirements, as per A2.
   * Screenshots as per examples below.

#### Assignment Screenshots:

Screenshot of payroll_calculator application running:

![image 1](images/payroll_calculator1.png)

Second screenshot of payroll_calculator application running:

![image 2](images/payroll_calculator2.png)