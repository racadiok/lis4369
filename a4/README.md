> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### Assignment 4 Requirements:

1. Run demo.py
2. If errors, more than likely missing installations.
3. Test Python Package Installer: pip freeze
4. Research how to install any missing packages:
    a. pandas (only if missing)
    b. pandas-datareader (only if missing)
    c. matplotlib (only if missing)
5. Create at least three functions that are called by the program:
    a. main(): calls at least two other functions.
    b. get_requirements_2(): displays the program requirements.
    c. data_analysis_2(): displays results as per demo.py.
6.Display graph as per instructions w/in demo.py


#### README.md file should include the following items:

   * Assignment requirements, as per A4.
   * Screenshots as per examples below.

#### Assignment Screenshots:

Screenshots of demo.py application running:

![image 1](images/2demo.png)

Graph:

![image 2](images/2demo2.png)