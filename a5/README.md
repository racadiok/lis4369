> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### Assignment 5 Requirements:

1. Complete the following tutorial: **Introduction to R Setup and Tutorial**
2. Code and run lis4369_a5.R
3. Be sure to include **at least two plots** in your **README.md file**.


#### README.md file should include the following items:

   * **Assignment requirements, as per A5**.
   * Screenshots of output from code below.

#### Assignment Screenshots:

Screenshot of Rstudio lis4369_a5.R running:

![image 1](images/Rstudio.png)

Screenshot of boxplot:

![image 2](images/boxplot.png)

Screenshot of histogram:

![image 3](images/histogram.png)