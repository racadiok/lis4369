# LIS4369 - Extensible Enterprise Solutions

## Kelly Racadio

### LIS4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create *a_1_tip_calculator* application
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
        
2. [A2 README.md](a2/README.md)
    - Must use float data type for user input.
    - Overtime rate: 1.5 times hourly rate (hours over 40).
    - Holiday rate: 2.0 times hourly rate (all holiday hours).
    - Must format currency with dollar sign, and round to two decimal places.
    - Create at least three functions that are called by the program:
        a. main(): calls at least two other functions.
        b. get_requirements(): displays the program requirements.
        c. calculate_payroll(): calculates an individual one-week paycheck.
        
3. [A3 README.md](a3/README.md)
    - Calculate home interior paint cost (w/o primer).
    - Must use float data types.
    - Must use SQFT_PER_GALLON constant (350).
    - Must use iteration structure (aka "loop").
    - Format, right-align numbers, and round to two decimal places.
    - Create at least three functions that are called by the program:
        a. main(): calls at least two other functions.
        b. get_requirements(): displays the program requirements.
        c. calculate_payroll(): calculates interior home painting.

        
4. [P1 README.md](p1/README.md)
    - Requirements:
        a. Code and run demo.py
        b. Then, use it to backward-engineer the screenshots below it.
    - Be sure to test your program using both IDLE and Visual Studio Code.
        
5. [A4 README.md](a4/README.md)
    - Run demo.py
    - If errors, more than likely missing installations.
    - Test Python Package Installer: pip freeze
    - Research how to install any missing packages:
        a. pandas (only if missing)
        b. pandas-datareader (only if missing)
        c. matplotlib (only if missing)
    - Create at least three functions that are called by the program:
        a. main(): calls at least two other functions.
        b. get_requirements_2(): displays the program requirements.
        c. data_analysis_2(): displays results as per demo.py.
    - Display graph as per instructions w/in demo.py
        
6. [A5 README.md](a5/README.md)
    - Complete the following tutorial: **Introduction to R Setup and Tutorial**
    - Code and run lis4369_a5.R
    - Be sure to include **at least two plots** in your **README.md file**.
        
7. [P2 README.md](a6/README.md)
    - Use Assignment 5 screenshots and references above for the following requirements:
    - Backward-engineer the **lis4369_p2_requirements.txt** file.
    - Be sure to include **at least two plots** in your **README.md file**.
    - Be sure to test your program using *RStudio*.